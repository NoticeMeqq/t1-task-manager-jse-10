package ru.t1.annenkovsv.tm.api.service;

import ru.t1.annenkovsv.tm.model.Task;
import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    void clear();

}
