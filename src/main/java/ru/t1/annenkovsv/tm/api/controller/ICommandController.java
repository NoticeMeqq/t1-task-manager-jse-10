package ru.t1.annenkovsv.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showAbout();

    void showVersion();

    void showMemoryInfo();

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

}
