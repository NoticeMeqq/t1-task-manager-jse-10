package ru.t1.annenkovsv.tm.api.repository;

import ru.t1.annenkovsv.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

}
