package ru.t1.annenkovsv.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProject();

}
