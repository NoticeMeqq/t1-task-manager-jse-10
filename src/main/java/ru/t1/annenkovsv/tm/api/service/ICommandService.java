package ru.t1.annenkovsv.tm.api.service;

import ru.t1.annenkovsv.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

    String getArgumentsValue();

    String getCommandsValue();

}
