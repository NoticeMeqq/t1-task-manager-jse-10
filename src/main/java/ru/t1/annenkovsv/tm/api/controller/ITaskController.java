package ru.t1.annenkovsv.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTask();

}
