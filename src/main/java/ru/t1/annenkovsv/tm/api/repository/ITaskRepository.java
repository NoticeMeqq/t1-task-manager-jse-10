package ru.t1.annenkovsv.tm.api.repository;

import ru.t1.annenkovsv.tm.model.Task;
import java.util.List;

public interface ITaskRepository {

    void add(Task project);

    List<Task> findAll();

    void clear();

}
