package ru.t1.annenkovsv.tm.constant;

public final class CommandConst {

    public static final String HELP = "HELP";

    public static final String VERSION = "VERSION";

    public static final String ABOUT = "ABOUT";

    public static final String INFO = "INFO";

    public static final String EXIT = "EXIT";

    public static final String PROJECT_LIST = "LISTPROJECTS";

    public static final String PROJECT_CLEAR = "CLEARPROJECTS";

    public static final String PROJECT_CREATE = "CREATEPROJECT";

    public static final String TASK_LIST = "LISTTASKS";

    public static final String TASK_CLEAR = "CLEARTASKS";

    public static final String TASK_CREATE = "CREATETASK";

    private CommandConst() {
    }

}