package ru.t1.annenkovsv.tm.constant;

public final class InformationConst {

    public static final String CURRENTVERSION = "1.10.0";

    public static final String LASTUPDATED = "24.06.2022";

    public static final String DEVELOPERNAME = "Sergey Annenkov";

    public static final String EMAIL = "sannenkov@t1-consulting.ru";

    private InformationConst() {
    }

}