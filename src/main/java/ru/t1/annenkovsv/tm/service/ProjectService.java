package ru.t1.annenkovsv.tm.service;

import ru.t1.annenkovsv.tm.api.repository.IProjectRepository;
import ru.t1.annenkovsv.tm.api.service.IProjectService;
import ru.t1.annenkovsv.tm.model.Project;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        projectRepository.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
