package ru.t1.annenkovsv.tm.service;

import ru.t1.annenkovsv.tm.api.repository.ICommandRepository;
import ru.t1.annenkovsv.tm.api.service.ICommandService;
import ru.t1.annenkovsv.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String getArgumentsValue() {
        return commandRepository.getArgumentsValue();
    }

    @Override
    public String getCommandsValue() {
        return commandRepository.getCommandsValue();
    }

}
