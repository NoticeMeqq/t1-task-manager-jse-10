package ru.t1.annenkovsv.tm.repository;

import ru.t1.annenkovsv.tm.api.repository.ICommandRepository;
import ru.t1.annenkovsv.tm.constant.ArgumentConst;
import ru.t1.annenkovsv.tm.constant.CommandConst;
import ru.t1.annenkovsv.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "Current task manager version."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "Information on developer."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "Program arguments list."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "Memory usage and system information."
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null,
            "Show created projects list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null,
            "Clear created projects."
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null,
            "Show created tasks list."
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null,
            "Clear created tasks."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "Exit program."
    );

    private static final Command[] COMMANDS = new Command[] {
            INFO, ABOUT, HELP, VERSION, PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, TASK_CREATE, TASK_LIST, TASK_CLEAR, EXIT
    };

    public Command[] getCommands(){
        return COMMANDS;
    }

    public String getArgumentsValue(){
        StringBuilder result = new StringBuilder();
        for (Command command : COMMANDS) {
            if (command.getArgument() != null) {
                result.append(command.getArgument()).append(", ");
            }
        }
        result.replace(result.lastIndexOf(","), result.lastIndexOf(",")+1,".");
        return result.toString();
    }

    public String getCommandsValue(){
        StringBuilder result = new StringBuilder();
        for (Command command : COMMANDS) {
            if (command.getName() != null) {
                result.append(command.getName()).append(", ");
            }
        }
        result.replace(result.lastIndexOf(","), result.lastIndexOf(",")+1,".");
        return result.toString();
    }

}
