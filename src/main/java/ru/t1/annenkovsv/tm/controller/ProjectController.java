package ru.t1.annenkovsv.tm.controller;

import ru.t1.annenkovsv.tm.api.controller.IProjectController;
import ru.t1.annenkovsv.tm.api.service.IProjectService;
import ru.t1.annenkovsv.tm.model.Project;
import ru.t1.annenkovsv.tm.util.TerminalUtil;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATING PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[SUCCESS]");
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOWING PROJECTS]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
        }
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEARING PROJECTS]");
        projectService.clear();
        System.out.println("[SUCCESS]");
    }

}
